  exports.config= {

      framework: 'jasmine',
      seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
      specs: ['spec.js'],
      params: {
          url: 'https://etsy.com/'
      },
      onPrepare: function(){
         browser.ignoreSynchronization = true;
      }
  }
